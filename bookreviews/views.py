from django.contrib.auth.models import User
from rest_framework import viewsets
from bookreviews.serializers import UserSerializer, BookSerializer, BookReviewSerializer
from bookreviews.models import Book, BookReview


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookReviewListViewSet(viewsets.ModelViewSet):
    queryset = BookReview.objects.all()
    serializer_class = BookReviewSerializer


class BookReviewViewSet(viewsets.ModelViewSet):
    serializer_class = BookReviewSerializer

    def get_queryset(self, *args, **kwargs):
        return BookReview.objects.filter(user_id=self.kwargs['pk'])


