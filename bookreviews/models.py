from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    title = models.CharField(max_length=30)
    author = models.CharField(max_length=30)
    price = models.FloatField()

    def __str__(self):
        return self.title


class BookReview(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE)

    review = models.TextField()

    def __str__(self):
        return self.book_id.title + ' - ' + self.user_id.username




