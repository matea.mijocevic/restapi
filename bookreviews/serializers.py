from django.contrib.auth.models import User
from rest_framework import serializers
from bookreviews.models import Book, BookReview


# Serializers define the API representation.


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ['title', 'author', 'price', 'id']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff', 'id']


class BookReviewSerializer(serializers.HyperlinkedModelSerializer):
   # user_id = serializers.StringRelatedField()

    class Meta:
        model = BookReview
        fields = ['user_id', 'book_id', 'review', 'id', 'url']
        extra_kwargs = {
            'id': {'read_only': True},
        }



