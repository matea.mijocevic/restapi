## Django

### Quickstart

Steps:
 - clone repository
 - create virtualenv with ```virtualenv -p python3 venv```
 - activate virtualenv with ```source venv/bin/activate```
 - install requirements with ```pip install -r requirements.txt```
 - migrate database
  ```python manage.py makemigrations```
  ```python manage.py migrate```
 - runserver with ```python manage.py runserver```


Basic commands:

```bash
python manage.py runserver # pokreće server
python manage.py makemigrations # stvara migracije za novostvorene modele
python manage.py migrate # migrira stvorene migracije
python manage.py createsuperuser # stvara admin usera
```

Must read:
 - [django docs](https://docs.djangoproject.com/en/2.1/)
 - [djangorestframework](https://www.django-rest-framework.org/tutorial/quickstart/)

